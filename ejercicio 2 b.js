function numero_alumnos( profesor, clave )
{
    var grupos = profesor["grupos"];
    for ( var i = 0; i < grupos.length; i++ )
    {
        if( grupos[i]["clave"] == clave )
            return grupos[i]["alumnos"].length;
    }
    return 0;
}

console.log( numero_alumnos(prof, "0"));

prof = {
    "clave" : "1",
    "nombre" : "Pedro Lopez",
    "grupos" : 
        [
            {
                "clave" : "1",
                "nombre" : "A",
                "hora" : "11:00 - 12:00",
                "alumnos" : 
                    [
                        "Luis Hdez",
                        "Carlos Perez",
                        "karla Marquez"
                    ]
            },
            {
                "clave" : "2",
                "nombre" : "B",
                "hora" : "12:00 - 13:00",
                "alumnos" : 
                    [
                        "Luis Hdez",
                        "Carlos Perez",
                    ]
            }
        ]
};
